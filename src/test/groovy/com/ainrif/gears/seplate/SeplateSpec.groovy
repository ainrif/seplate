/*
 * Copyright 2014-2017 Ainrif <support@ainrif.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ainrif.gears.seplate

import com.ainrif.gears.seplate.model.CrudEntity
import com.ainrif.gears.seplate.model.DefaultFieldsEntity
import com.ainrif.gears.seplate.model.DeleteEntity
import com.ainrif.gears.seplate.model.FindAllByFilterEntity
import com.ainrif.gears.seplate.model.FindAllEmptyEntity
import com.ainrif.gears.seplate.model.FindAllEntity
import com.ainrif.gears.seplate.model.FindByIdEntity
import com.ainrif.gears.seplate.model.FindEntity
import com.ainrif.gears.seplate.model.InsertEntity
import com.ainrif.gears.seplate.model.SaveEntity
import com.ainrif.gears.seplate.model.TransactionalEntity
import com.ainrif.gears.seplate.model.TypesCheckEntity
import groovy.sql.Sql
import org.h2.jdbcx.JdbcConnectionPool
import spock.lang.Specification

import javax.sql.DataSource
import java.sql.SQLException
import java.time.LocalDateTime

class SeplateSpec extends Specification {
    DataSource ds = JdbcConnectionPool.create('jdbc:h2:mem:repo_spec', '', '')
    Sql sql = new Sql(ds)

    def "insert should persist new entity and return auto-generated id"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE insert_entity(
                id    BIGINT AUTO_INCREMENT PRIMARY KEY,
                login VARCHAR(256) UNIQUE,
            );
        """)

        when:
        def actual = repo.insert(new InsertEntity(login: 'l'))

        then:
        actual == 1L
        sql.firstRow(id: actual, "SELECT * FROM insert_entity WHERE login = 'l' AND id = :id;")

        when: "try to insert entity with filled id"
        repo.insert(new InsertEntity(login: 'l'))

        then:
        thrown(SQLException)
    }

    def "find of one item should support filter by fields"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE find_entity(
                id    BIGINT AUTO_INCREMENT PRIMARY KEY,
                login VARCHAR(256) UNIQUE,
                profile_version   INT, 
                ignored INT
            );
            
            INSERT INTO find_entity (id, login, profile_version, ignored) VALUES (1, 'l', 20, 42)
        """)
        when:
        def actual = repo.findOne(FindEntity, profileVersion: 20, login: 'l', 'profileVersion < :profileVersion and login = :login')

        then:
        !actual

        when:
        actual = repo.findOne(FindEntity, profileVersion: 20, login: 'l', 'profileVersion >= :profileVersion and login = :login')

        then:
        actual
    }

    def "check types; these types should be de- and serialised properly"() {
        given:
        def localDateTime = LocalDateTime.parse('2000-01-01T01:01:01')

        def entity = new TypesCheckEntity(jLocalDateTime: localDateTime)

        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE types_check_entity(
                id    BIGINT AUTO_INCREMENT PRIMARY KEY,
                j_local_date_time TIMESTAMP,
            );
        """)
        when:
        def saved = repo.save(entity)
        def updated = repo.findById(saved.id, TypesCheckEntity)

        then:
        saved.jLocalDateTime == localDateTime
        and:
        updated.jLocalDateTime == localDateTime
    }

    def "should be able to find all items in table"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE find_all_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE
            );
            
            INSERT INTO find_all_entity (id, login) VALUES (1, 'l1');
            INSERT INTO find_all_entity (id, login) VALUES (2, 'l2');
            INSERT INTO find_all_entity (id, login) VALUES (3, 'l3');
        """)
        when:
        def actual = repo.findAll(FindAllEntity)

        then:
        actual.size() == 3
        actual*.id.sum() == 6
    }

    def "should be able to find many results by filter"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE find_all_by_filter_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE
            );
            
            INSERT INTO find_all_by_filter_entity (id, login) VALUES (1, 'l1');
            INSERT INTO find_all_by_filter_entity (id, login) VALUES (2, 'l2');
            INSERT INTO find_all_by_filter_entity (id, login) VALUES (3, 'l3');
        """)

        when:
        def actual = repo.findAll(id: 2, FindAllByFilterEntity, 'id >= :id')

        then:
        actual.size() == 2
        actual*.id.sum() == 5

        when:
        actual = repo.findAll(id: 6, FindAllByFilterEntity, 'id >= :id')

        then:
        actual.size() == 0
    }

    def "find for several items should work properly in case of empty table"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE find_all_empty_entity (
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE
            );
        """)

        when:
        def actual = repo.findAll(FindAllEmptyEntity)

        then:
        actual.empty

        when:
        actual = repo.findAll(id: 2, FindAllEmptyEntity, 'id >= :id')

        then:
        actual.empty
    }

    def "save should return updated input object"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE save_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE,
            );
        """)

        def input = new SaveEntity(login: 'l')

        when:
        def actual = repo.save(input)

        then:
        actual.is(input)
        actual.id == 1
        actual.login == 'l'

        when:
        input.login = 'l2'
        actual = repo.save(input)

        then:
        actual.is(input)
        actual.id == 1
        actual.login == 'l2'
    }

    def "delete should remove record by id"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE delete_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE
            );
            
            INSERT INTO delete_entity (id, login) VALUES (1, 'l')
        """)

        when:
        repo.delete(new DeleteEntity(id: 1))

        then:
        !sql.firstRow("SELECT * FROM delete_entity WHERE login = 'l' AND id = 1;")
    }

    def "findById should return record by id"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE find_by_id_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE
            );
            
            INSERT INTO find_by_id_entity (id, login) VALUES (1, 'l')
        """)

        when:
        def actual = repo.findById(1, FindByIdEntity)

        then:
        actual
        actual instanceof FindByIdEntity
        actual.id == 1
        actual.login == 'l'
    }

    def "default values should be used for properly annotated fields"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE default_fields_entity(
                id                          BIGINT AUTO_INCREMENT PRIMARY KEY,
                annotated_default_field     VARCHAR(256) DEFAULT 'used default',
                not_annotated_default_field VARCHAR(256) DEFAULT 'not used default'
            );
        """)

        when:
        def actual = repo.save(new DefaultFieldsEntity())

        then:
        actual.id == 1
        actual.annotatedDefaultField == 'used default'
        actual.notAnnotatedDefaultField == null
    }

    def "CRUD should work for any object"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE crud_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE,
            );
        """)

        def input = new CrudEntity(login: 'l')

        when: "create record"
        def id = repo.insert(input)

        then:
        id

        when: "read record"
        def entity = repo.findById(id, CrudEntity)

        then:
        entity
        entity.login == 'l'

        when: "update record"
        entity.login = 'l2'
        repo.save(entity)

        then:
        entity.id == id
        entity.login == 'l2'

        when: "delete entity"
        repo.delete(entity)

        then:
        !repo.findById(id, CrudEntity)
    }

    def "seplate should support transactions"() {
        given:
        def repo = new Seplate(ds)

        sql.execute("""
            CREATE TABLE transactional_entity(
                id            BIGINT AUTO_INCREMENT PRIMARY KEY,
                login         VARCHAR(256) UNIQUE,
            );
        """)

        when:
        def id = null
        repo.withTransaction {
            id = insert(new TransactionalEntity(login: 'l'))
            throw new RuntimeException()
        }

        then:
        thrown(RuntimeException)
        id
        !repo.findById(id, TransactionalEntity)
    }
}
