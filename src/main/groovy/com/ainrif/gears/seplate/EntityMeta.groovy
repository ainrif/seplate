/*
 * Copyright 2014-2017 Ainrif <support@ainrif.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ainrif.gears.seplate

import groovy.transform.PackageScope
import org.apache.commons.lang3.reflect.FieldUtils

import java.lang.reflect.Field

import static com.ainrif.gears.seplate.SeplateUtils.findIdField
import static com.ainrif.gears.seplate.SeplateUtils.toPythonCase
import static java.util.Collections.singletonMap
import static org.apache.commons.lang3.reflect.FieldUtils.readField

@PackageScope
class EntityMeta {

    Class<?> type
    String tableName
    Field idField
    String idColumnName
    Object idValue
    Map<String, String> fieldToColumnNameMapping

    EntityMeta(Class<?> type) {
        this.type = type
        this.tableName = toPythonCase(type.simpleName)
        this.idField = findIdField(type)
        this.idColumnName = toPythonCase(idField.name)
    }

    EntityMeta(Object entity) {
        this(entity.class)
        this.idValue = readField(idField, entity, true)
    }

    Optional<Object> getIdValue() {
        return idValue ? Optional.of(idValue) : Optional.empty()
    }

    Map<String, String> getFieldToColumnNameMapping() {
        return fieldToColumnNameMapping ?: {
            fieldToColumnNameMapping = FieldUtils.getAllFieldsList(type)
                    .findAll { !it.synthetic }
                    .collectEntries { singletonMap(it.name, toPythonCase(it.name)) }
        }()
    }
}
