/*
 * Copyright 2014-2017 Ainrif <support@ainrif.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ainrif.gears.seplate

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.apache.commons.lang3.reflect.FieldUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.annotation.Nullable
import javax.sql.DataSource
import java.sql.SQLException
import java.time.temporal.UnsupportedTemporalTypeException

import static com.ainrif.gears.seplate.SeplateUtils.toPythonCase
import static java.util.Collections.singletonMap
import static org.apache.commons.lang3.Validate.validState
import static org.apache.commons.lang3.reflect.FieldUtils.readField

/**
 * Simple rEPository tempLATE
 */
class Seplate {
    static final Logger logger = LoggerFactory.getLogger(Seplate)

    private final DataSource dataSource
    protected final Sql sql

    Seplate(DataSource dataSource) {
        this.dataSource = dataSource
        this.sql = new Sql(dataSource)
    }

    /**
     * @see Sql#withTransaction(groovy.lang.Closure)
     * @param closure
     * @throws SQLException
     */
    void withTransaction(@DelegatesTo(SeplateTx) Closure closure) throws SQLException {
        closure = closure.clone() as Closure
        def txRepo = new SeplateTx(dataSource)

        closure.delegate = txRepo
        closure.resolveStrategy = Closure.DELEGATE_FIRST

        txRepo.sql.withTransaction(closure)
    }

    // todo
    def <T> List<T> findAll(Class<T> type) {
        return sql.rows("select * from ${toPythonCase(type.simpleName)};".toString())
                .findAll { it }
                .collect { mapRowResultToPogo(type.newInstance(), it) }
    }

    // todo
    def <T> List<T> findAll(Map<String, Object> params, Class<T> type, String where) {
        def meta = new EntityMeta(type)
        String resultWhere = meta.fieldToColumnNameMapping.inject(where) { String w, String f, String c ->
            w.replace(f, c)
        }

        Map<String, Object> resultParams = params.collectEntries { singletonMap(toPythonCase(it.key), it.value) }

        def querySql = "select * from ${meta.tableName} where ${resultWhere};"

        sql.rows(resultParams, querySql)
                .collect { mapRowResultToPogo(type.newInstance(), it) }
    }

    @Nullable
    <T> T findOne(Map<String, Object> params, Class<T> type, String where) {
        def meta = new EntityMeta(type)
        String resultWhere = meta.fieldToColumnNameMapping.inject(where) { String w, String f, String c ->
            w.replace(f, c)
        }

        Map<String, Object> resultParams = params.collectEntries { singletonMap(toPythonCase(it.key), it.value) }

        def querySql = "select * from ${meta.tableName} where ${resultWhere};"

        return findNative(resultParams, type, querySql)
    }

    def <T> T findNative(Map<String, Object> params, Class<T> resultType, String querySql) {
        def row = sql.firstRow(params, querySql)

        return row ? mapRowResultToPogo(resultType.newInstance(), row) : null
    }

    /**
     * @param entity to insert
     * @return auto-generated key
     */
    def <T> Object insert(T entity) {
        def meta = new EntityMeta(entity)

        validState(!readField(meta.idField, entity, true))

        def fieldMap = collectFieldValues(meta.idField, entity)
        def valuesTemplate = createFieldInsertTemplate(fieldMap)

        def insert = sql.executeInsert(fieldMap, "insert into ${meta.tableName} ${valuesTemplate};")

        return insert[0][0]
    }

    def <T> T save(final T entity) {
        def meta = new EntityMeta(entity)

        if (!meta.idValue.present) {
            meta.idValue = insert(entity)
        } else {
            def fieldMap = collectFieldValues(meta.idField, entity)
            def valuersTemplate = createFieldUpdateTemplate(fieldMap)

            fieldMap.put(meta.idColumnName, meta.idValue.get())
            def updatedCount = sql.executeUpdate(fieldMap, "update ${meta.tableName} set ${valuersTemplate} where ${meta.idColumnName} = :${meta.idColumnName};")
            validState(updatedCount == 1)
        }

        def row = sql.firstRow([(meta
                .idColumnName): meta.idValue.get()], "select * from ${meta.tableName} where ${meta.idColumnName} = :${meta.idColumnName};")

        return mapRowResultToPogo(entity, row)
    }

    def <T> void delete(T entity) {
        def type = entity.class
        def tableName = toPythonCase(type.simpleName)
        def idField = SeplateUtils.findIdField(type)
        def idColumnName = toPythonCase(idField.name)
        def idValue = readField(idField, entity, true)

        sql.execute(singletonMap(idColumnName, idValue), "DELETE FROM ${tableName} WHERE ${idColumnName} = :${idColumnName};")
    }


    @Nullable
    <T> T findById(Object idValue, Class<T> type) {
        def tableName = toPythonCase(type.simpleName)
        def idField = SeplateUtils.findIdField(type)
        def idColumnName = toPythonCase(idField.name)

        def row = sql.firstRow([(idColumnName): idValue], "select * from ${tableName} where ${idColumnName} = :${idColumnName};")

        return row ? mapRowResultToPogo(type.newInstance(), row) : null
    }

    private static <T> Map<String, Object> collectFieldValues(idField, T entity) {
        return FieldUtils.getAllFieldsList(entity.class)
                .findAll { !it.synthetic }
                .findAll { it.getAnnotation(DoNotInsertNull) ? readField(it, entity, true) != null : true }
                .findAll { it != idField }
                .collectEntries { [toPythonCase(it.name), readField(it, entity, true)] }
    }

    @SuppressWarnings("UnnecessaryQualifiedReference")
    private static <T> T mapRowResultToPogo(final T entity, GroovyRowResult rs) {
        def type = entity.class
        rs.each { k, v ->
            def fieldName = k.asType(String)
                    .toLowerCase()
                    .with SeplateUtils.&toCamelCase

            def field = FieldUtils.getField(type, fieldName, true)

            if (field) {
                def value = v
                if (v instanceof java.util.Date) {
                    switch (v.class) {
                        case java.sql.Date:
                            value = v.asType(java.sql.Date).toLocalDate()
                            break
                        case java.sql.Time:
                            value = v.asType(java.sql.Time).toLocalTime()
                            break
                        case java.sql.Timestamp:
                            value = v.asType(java.sql.Timestamp).toLocalDateTime()
                            break
                        default:
                            throw new UnsupportedTemporalTypeException("There are no mapping for type ${v.class}")
                    }
                }
                FieldUtils.writeField(field, entity, value, false)
            } else {
                logger.debug("Field {} is absent in class {}. Skip", fieldName, type.simpleName)
            }
        }

        return entity
    }

    private static String createFieldUpdateTemplate(Map<String, Object> fieldMap) {
        def valuesBuilder = new StringBuilder()

        fieldMap.each { k, v ->
            valuesBuilder.append(k)
                    .append(' = :')
                    .append(k)
                    .append(',')
        }

        return valuesBuilder
                .deleteCharAt(valuesBuilder.size() - 1)
                .toString()
    }


    private static String createFieldInsertTemplate(Map<String, Object> fieldMap) {
        def columnsBuilder = new StringBuilder('(')
        def valuesBuilder = new StringBuilder('(')

        fieldMap.each { k, v ->
            columnsBuilder.append(k).append(',')
            valuesBuilder.append(':').append(k).append(',')
        }

        columnsBuilder
                .deleteCharAt(columnsBuilder.size() - 1)
                .append(')')
        valuesBuilder
                .deleteCharAt(valuesBuilder.size() - 1)
                .append(')')

        return columnsBuilder.append(' values ')
                .append(valuesBuilder)
                .toString()
    }
}
