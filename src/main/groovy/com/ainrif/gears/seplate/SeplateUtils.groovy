/*
 * Copyright 2014-2017 Ainrif <support@ainrif.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ainrif.gears.seplate

import groovy.transform.PackageScope
import org.apache.commons.lang3.Validate
import org.apache.commons.lang3.reflect.FieldUtils

import java.lang.reflect.Field

@PackageScope
class SeplateUtils {
    static String toCamelCase(String text, boolean capitalized = false) {
        text = text.replaceAll("(_)([A-Za-z0-9])", { it[2].toUpperCase() })
        return capitalized ? text.capitalize() : text
    }

    static String toPythonCase(String text) {
        text.replaceAll(/([A-Z])/, /_$1/).toLowerCase().replaceAll(/^_/, '')
    }

    static Field findIdField(Class<?> type) {
        def idSearchResult = FieldUtils.getFieldsListWithAnnotation(type, Id)
        Validate.validState(idSearchResult && idSearchResult.size() == 1)

        return idSearchResult[0]
    }
}
